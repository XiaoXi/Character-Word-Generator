# Character Word Generator

字符生成字符字工具，你搁这搁这呢？

[![vercel](https://vercelbadge.soraharu.com/?app=characterwordgenerator)](https://characterwordgenerator.soraharu.com/)

## 🌎 项目首页

[https://characterwordgenerator.soraharu.com/](https://characterwordgenerator.soraharu.com/)

## 📜 开源许可

基于 [MIT License](https://choosealicense.com/licenses/mit/) 许可进行开源。
